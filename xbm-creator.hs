import Data.List (intersperse)
import Numeric (showHex)

main = do
    text <- fmap (map (map (== '#')) . lines) $ getContents
    let width = maximum $ map length text
    putStrLn $ "#define width " ++ show width
    let height = length text
    putStrLn $ "#define height " ++ show height
    let realWidth = if width `mod` 8 == 0 then width else 8 * (width `div` 8 + 1)
    let alignedText = map (\line -> line ++ replicate (realWidth - length line) False) text
    putStrLn "static unsigned char bits[] = {"
    putStrLn $ concat $ intersperse ", " $ map (("0x" ++) . flip showHex "") $ toInts $ concat alignedText
    putStrLn "};"

toInts :: [Bool] -> [Int]
toInts [] = []
toInts bits = (sum $ map fst $ filter (id . snd) $ zip (map (2^) [0..]) $ take 8 bits):(toInts $ drop 8 bits)
