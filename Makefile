XBMSOURCES := $(wildcard text/*)
XBM := $(XBMSOURCES:text/%=xbm/%.xbm)

all: xbm-creator $(XBM)

xbm-creator: xbm-creator.hs
	ghc --make -O2 xbm-creator.hs
	rm xbm-creator.o xbm-creator.hi

xbm/%.xbm: text/% xbm-creator
	./xbm-creator < $< > $@
